package cheng.com.myapplication;
import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.os.SystemClock;
import android.provider.Settings;
import android.util.Log;

import androidx.annotation.Nullable;

import static cheng.com.myapplication.MainActivity.KEY_TASK;
import static cheng.com.myapplication.MainActivity.TASK_SERVICE;

public class NewService extends Service {


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent!= null){
            int type = intent.getIntExtra(KEY_TASK,TASK_SERVICE);
            if (type == TASK_SERVICE){
                Thread worker = new Thread(){
                    @Override
                    public void run() {
                        for (int i = 1; i <= 1000000; i++) {
                            Log.d("TASK_SERVICE: ",String.valueOf(i));
                            if(i==1000000){
                                stopSelf();
                            }
                        }
                    }
                };
                worker.start();
            }else {
                Thread worker = new Thread(){
                    @Override
                    public void run() {
                        for (int i = 2000000; i <= 3000000; i++) {
                            Log.d("TASK_INTENT_SERVICE: ",String.valueOf(i));
                            if(i==3000000) {
                                stopSelf();
                            }
                        }
                    }
                };
                worker.start();

            }

        }

        return super.onStartCommand(intent, flags, startId);

    }



    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}

