package cheng.com.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    // declaring objects of Button class
    private Button start, stop;
    public static final String KEY_TASK = "KEY_TASK";
    public static final int TASK_SERVICE = 1;
    private static final int TASK_INTENT_SERVICE = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // assigning ID of startButton
        // to the object start
        start = (Button) findViewById( R.id.startButton );

        // assigning ID of stopButton
        // to the object stop
        stop = (Button) findViewById( R.id.stopButton );

        // declaring listeners for the
        // buttons to make them respond
        // correctly according to the process
        start.setOnClickListener( this );
        stop.setOnClickListener( this );
    }

    public void onClick(View view) {

        // process to be performed
        // if start button is clicked
        if(view == start){

            // starting the service
            Intent intent = new Intent( this, NewService.class );
            intent.putExtra(KEY_TASK, TASK_SERVICE);
            startService(intent);
//            stopService(new Intent( this, NewService.class ) );
            return;

        }

        // process to be performed
        // if stop button is clicked
         if (view == stop){

            // stopping the service
            Intent intent = new Intent( this, NewService.class );
            intent.putExtra(KEY_TASK, TASK_INTENT_SERVICE);
            startService(intent);

        }
    }
}

